(defproject wuyu-api "0.1.0-SNAPSHOT"
  :description "API for wuyu.ws"
  :url "https://wuyu.ws"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/data.json "0.2.6"]
                 [http-kit "2.2.0"]
                 [ring/ring-devel "1.5.0"]]
  :main ^:skip-aot wuyu-api.core
  :jvm-opts ["-XX:-MaxFDLimit"]
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}
             :dev {:java-source-paths ["test/java"]
                   :dependencies [[io.netty/netty-all "4.1.7.Final"]
                                  [org.clojure/tools.namespace "0.2.11"]]}})
