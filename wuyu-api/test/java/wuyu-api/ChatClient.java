/*
 * For integration test
 */

package wuyu_api;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.Channel;
import io.netty.bootstrap.Bootstrap;
import io.netty.handler.codec.http.websocketx.*;
import io.netty.handler.codec.http.HttpClientCodec;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpResponse;

import java.net.URI;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;

class ChatHandler extends ChannelInboundHandlerAdapter {
    private final WebSocketClientHandshaker handshaker;

    public ChatHandler(WebSocketClientHandshaker hs) {
        this.handshaker = hs;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {

        if (msg instanceof TextWebSocketFrame) {
            TextWebSocketFrame frame = (TextWebSocketFrame) msg;
            //System.out.println(frame.text());
        }
    }

    @Override 
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) {
        if (evt == WebSocketClientProtocolHandler.ClientHandshakeStateEvent.HANDSHAKE_ISSUED) {
            //System.out.println("ISSUED!");
        }
        if (evt == WebSocketClientProtocolHandler.ClientHandshakeStateEvent.HANDSHAKE_COMPLETE) {
            //System.out.println("COMPLETED!!");
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        ctx.close();
    }
}

public class ChatClient {

    private Channel channel;
    private NioEventLoopGroup workers;

    public ChatClient(String url) throws Exception {
        this(url, new NioEventLoopGroup());
    }

    public ChatClient(String url, NioEventLoopGroup grp) throws Exception {
       URI uri = new URI(url);
       workers = grp;

       WebSocketClientHandshaker handshaker = WebSocketClientHandshakerFactory
           .newHandshaker(uri, WebSocketVersion.V13, null, false, null);

       Bootstrap b = new Bootstrap();

       b.group(workers);
       b.channel(NioSocketChannel.class);
       b.option(ChannelOption.SO_REUSEADDR, true);

       b.handler(new ChannelInitializer<NioSocketChannel>() {

           @Override
           public void initChannel(NioSocketChannel ch) throws Exception {
               ch.pipeline().addLast("codec", new HttpClientCodec());
               ch.pipeline().addLast("aggregator", 
                       new HttpObjectAggregator(8192));

               ch.pipeline().addLast("ws-handler", 
                       new WebSocketClientProtocolHandler(handshaker));
               ch.pipeline().addLast("chat-handler", 
                       new ChatHandler(handshaker));
           }
       });

       //System.out.print("Connecting to: ");
       //System.out.println(uri.toString());
       //ChannelFuture ff = b.bind(new InetSocketAddress("192.168.0.1", 0)).sync();
       //System.out.println(ff.channel().localAddress());
       ChannelFuture f = b.connect(uri.getHost(), uri.getPort()).sync(); 

       this.channel = f.channel();
       //this.channel.closeFuture().sync();
    }

    public void sendChat(String msg) throws Exception {
        String data = String.format("{\"type\": \"message\", \"data\": \"%s\"}", msg);
        //System.out.println("Here Sending: " + data);
        channel.writeAndFlush(new TextWebSocketFrame(data)).sync();
    }

    public void sendChatAsync(String msg) throws Exception {
        String data = String.format("{\"type\": \"message\", \"data\": \"%s\"}", msg);
        //System.out.println("Here Sending: " + data);
        channel.writeAndFlush(new TextWebSocketFrame(data));
    }

    public void closeChat() throws Exception {
        channel.close().sync();
        workers.shutdownGracefully();
    }

    public static void main(String[] args) throws Exception {
        try {
            System.out.println("Chat client running...");
            ChatClient client = new ChatClient("ws://0.0.0.0:8081/?topic=ok");
            System.out.println("Connected!");

            BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
            while (true) {
                String msg = console.readLine();
                if (msg == null) {
                    client.closeChat();
                    break;
                }
                //client.sendChat(msg);
                client.sendChatAsync(msg);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
