(ns wuyu-api.lobby-test
  (:require [clojure.test :refer :all]
            [wuyu-api.lobby :refer :all]))

(defn create-lbs []
  (swap! topic->lobby assoc "TEST_TP" (->Lobby "TEST_TP" 3 #{1 2})))

(defn destroy-lbs []
  (reset! topic->lobby {}))

(defn with-lbs [f] 
  (create-lbs)
  (f)
  (destroy-lbs))

(use-fixtures :each with-lbs)

(deftest alter-lobby-users-test
  (let [l (make-lobby {:users #{2}})]
    (testing "into-lobby works"
      (is (= (->Lobby nil 2 #{1 2}) (into-lobby l 1))))
    (testing "out-lobby works"
      (is (= (->Lobby nil 2 #{})) (out-lobby l 2)))))

(deftest make-lobby-test
  (testing "fully supplid"
    (is (= (->Lobby "OH" 3 #{1 2})
           (make-lobby {:topic "OH" :size 3 :users #{1 2}}))))
  (testing "optional parameters"
    (is (= (->Lobby nil 2 #{})
           (make-lobby {})))))

(deftest register-test 
  (testing "register! can join"
    (is (= (register! "TEST_TP" 3)
           (->Lobby "TEST_TP" 3 #{1 2 3})))
    (is (= (:users (get-lobby "TEST_TP"))
           #{1 2 3})))
  (testing "register! can create"
    (register! "TEST_TP2" 4)
    (is (= (:users (get-lobby "TEST_TP2"))
           #{4})))
  (testing "register! can throw exception when lobby is full"
    (is (thrown? clojure.lang.ExceptionInfo (register! "TEST_TP" 4)))))

(deftest quit-test 
  (is (= (->Lobby "TEST_TP" 3 #{2}) (quit! "TEST_TP" 1)))
  (is (= #{2} (:users (get-lobby "TEST_TP"))))
  (quit! "TEST_TP" 2)
  (is (nil? (quit! "TEST_TP" 2)))
  (quit! "TEST_TP" 4)
  (is (nil? (get-lobby "TEST_TP"))))
