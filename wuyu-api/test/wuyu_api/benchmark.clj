(ns wuyu-api.benchmark
  (:require [clojure.test :refer :all]
            [wuyu-api.core :as core]
            [clojure.data.json :as json])
  (:import [wuyu_api ChatClient]
           [java.util.concurrent Executors TimeUnit]
           [io.netty.channel.nio NioEventLoopGroup]))

(def all-chars (map char (range 66 92)))

(defn rand-char [] (rand-nth all-chars))

(defn rand-str [n] (apply str (take (rand-int n) (repeatedly rand-char))))

(defn -main [ncon & args]
  (let [ncon (read-string ncon)
        workers (NioEventLoopGroup. 2)
        sockets (map 
                  (fn [t] 
                    (when (zero? (mod t 200)) 
                      ; 2000 connections per seconds
                      (Thread/sleep 100) 
                      (println "Connecting " t))
                    (ChatClient. 
                      (str "ws://0.0.0.0:8081/?topic=" (int (/ t 2)))
                      workers)) 
                  (range ncon))
        tasks (map (fn [s]
                     (let [msg (rand-str 128)]
                       #(.sendChatAsync s msg)))
                   sockets)
        exec (Executors/newScheduledThreadPool 2)]
    (doseq [t tasks]
      (.scheduleAtFixedRate exec t 1 (+ 10 (rand-int 60)) TimeUnit/SECONDS))
    ))
