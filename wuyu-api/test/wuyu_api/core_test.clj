(ns wuyu-api.core-test
  (:require [clojure.test :refer :all]
            [wuyu-api.core :refer :all]
            [wuyu-api.lobby :as lb]
            [clojure.data.json :as json]
            [org.httpkit.server :refer [send! close Channel]]))

(defmacro with-mock [vr & body] 
  `(with-redefs [~vr (with-meta 
                       (fn [& ~'args] (swap! (-> ~vr meta :args) conj ~'args))
                       {:args (atom [])})] 
     (do ~@body)))

(defn mock-args [f] 
  (-> f meta :args deref))

(defn setup-mock [f]
  (->>
    (f)
    (with-mock send!)
    (with-mock close)))

(use-fixtures :each setup-mock)

(deftest broadcast-test 
  (let [l (lb/->Lobby "" 3 #{1 2 3})]
    (broadcast! "test" l)
    (is (map = (mock-args send!) 
             [[1 "test"] 
              [2 "test"] 
              [3 "test"]]))))

(deftest end-chan-test 
  (with-mock broadcast! 
    (with-redefs [lb/quit! (constantly (lb/->Lobby "" 3 #{1 2}))]
      (end-chan "" 1)
      (is (= (-> broadcast! mock-args first first) 
             (json/write-str (:userleft proto)))))))

(deftest init-chan-test 
  (with-mock broadcast!
    (with-redefs [lb/register! (constantly (lb/->Lobby "" 2 #{1}))
                  lb/user-count (constantly 1)]
      (is (true? (init-chan "" 1))))
    
    (with-redefs [lb/register! (constantly (lb/->Lobby "" 2 #{1 2}))]
      (init-chan "" 1)
      (is (= (-> broadcast! mock-args first first) 
             (json/write-str (:matched proto)))))

    (with-redefs [lb/register! (fn [& _] (throw (ex-info "test" {:type :lb-full})))
                  end-chan (constantly nil)]
      (init-chan "" 1)
      (is (= (-> send! mock-args first) 
             [1 (json/write-str (:occupied proto))])))))
