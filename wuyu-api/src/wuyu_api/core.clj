(ns wuyu-api.core
  (:require [ring.middleware.reload :as reload]
            [ring.middleware.params :refer [wrap-params]]
            [clojure.data.json :as json]
            [org.httpkit.server :refer :all]
            [wuyu-api.lobby :as lb])
  (:gen-class))

(def dev? (nil? (System/getenv "CLOJURE_PRODUCTION")))

(def port 8081)

; Static part of wuyu proto
(def proto {:matched {:type "meta"
                      :data "matched"}
            :occupied {:type "meta"
                       :data "occupied"}
            :userleft {:type "notification"
                       :data "userleft"}})


(defn broadcast! [data lobby & {:keys [exclude] :or {exclude #{}}}]
  (doseq [chan (:users lobby)]
    (when-not (exclude chan)
      (send! chan data))))

(defn end-chan [topic chan] 
  (if-let [lobby (lb/quit! topic chan)]
    (broadcast! (json/write-str (:userleft proto)) lobby))
  (close chan))

(defn init-chan [topic chan] 
  (try 
    (let [l (lb/register! topic chan)]
      (when (> (lb/user-count l) 1)
        (broadcast! (json/write-str (:matched proto)) l))) 
      true
    (catch clojure.lang.ExceptionInfo e
      (case (-> e ex-data :type)
         :lb-full (do (send! chan (json/write-str (:occupied proto)))
                      (end-chan topic chan))))))

(defn handler [req] 
  (with-channel req chan
    (let [tp (-> req :params (#(% "topic"))) ]
      (when (init-chan tp chan)
        (on-receive chan (fn [data] 
                           (if-let [l (lb/get-lobby tp)]
                             (broadcast! data l :exclude #{chan}))))
        (on-close chan (fn [status] 
                         (end-chan tp chan)))))))

(def app (wrap-params 
           (if dev? 
             (reload/wrap-reload #'handler)
             handler)))

(defn -main [& args]
  (println "Listening on port: " port)
  (when dev? 
    (println "Server runninng in development mode."))
  (run-server app {:port port}))
