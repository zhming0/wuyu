(ns wuyu-api.lobby)

(defrecord Lobby [topic size users])

(def topic->lobby (atom {}))

(defn get-lobby [topic] (@topic->lobby topic))

(defn make-lobby [{:keys [topic size users] :or {size 2 users #{}}}]
  (Lobby. topic size users))

(defn user-count [lobby] 
  (count (:users lobby)))

(defn alter-lobby-users [f]
  (fn [lobby chan]
    (let [ousers (:users lobby)]
      (assoc lobby :users (f ousers chan)))))

(def into-lobby (alter-lobby-users conj))

(def out-lobby (alter-lobby-users disj))

; Join / Create a lobby
(defn register! [topic chan]
  (let [l (or (get-lobby topic) (make-lobby {:topic topic}))]
    (if (< (user-count l) (:size l))
      (let [nl (into-lobby l chan)] 
        (swap! topic->lobby assoc topic nl)
        nl)
      (throw (ex-info "" {:type :lb-full})))))

(defn quit! [topic chan]
  (when-let [olobby (@topic->lobby topic)]
    (when ((:users olobby) chan)
      (let [nlobby (out-lobby olobby chan)]
        (swap! topic->lobby assoc topic nlobby)
        (when (= (user-count nlobby) 0)
          (swap! topic->lobby dissoc topic))
        nlobby))))
