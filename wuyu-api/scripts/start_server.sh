#!/bin/bash

export CLOJURE_PRODUCTION=true

java -server -XX:-MaxFDLimit -Xms256m -Xmx256m  \
    -cp `lein classpath` clojure.main \
    -m wuyu-api.core $@
