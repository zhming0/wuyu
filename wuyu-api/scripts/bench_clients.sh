#!/bin/bash

# Increase maxfiles for mac
# http://unix.stackexchange.com/questions/108174/how-to-persist-ulimit-settings-in-osx-mavericks

# Mac: port range per IP (10000 -> 65536)
# sudo sysctl -w net.inet.ip.portrange.first=10000

sysctl kern.maxfiles
sysctl kern.maxfilesperproc
ulimit -n
launchctl limit maxfiles 

java -XX:-MaxFDLimit -cp `lein classpath` clojure.main \
    -m wuyu-api.benchmark $@
