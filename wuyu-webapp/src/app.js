// Libraries
import React from 'react';
import ReactDOM from 'react-dom';

// CSS 
//import '!style-loader!css-loader!purecss/build/pure-min.css';
//import '!style-loader!css-loader!purecss/build/grids-responsive-min.css';
//import '!style-loader!css-loader!font-awesome/css/font-awesome.min.css';
import 'purecss/build/pure-min.css';
import 'purecss/build/grids-responsive-min.css';
import 'font-awesome/css/font-awesome.min.css';
import './css/app.css';

// Components
import Chat from './components/Chat';

const chatDOM = document.getElementById('chat-overlay');
const landingDOM = document.querySelector('.content-wrapper');
let showChat = true;

const back = function() {
  ReactDOM.render(<div />, chatDOM);
  //chatDOM.style.display = "block";
  landingDOM.style.display = "block";
}

const go = function(e) {
  e.preventDefault();
  const topic = document.querySelector('.main-input').value;
  if (topic.length === 0) return;

  ReactDOM.render(<Chat topic={topic} onClose={() => {
    back();
    //chatDOM.style.display = "none";
    //landingDOM.style.display = "block";
  }}/>, chatDOM);
  chatDOM.style.display = "block";
  landingDOM.style.display = "none";
  //console.log(topic);
}

chatDOM.style.display = "none";
document.querySelector('.main-button').addEventListener('click', go);
