import React from 'react';
import config from 'config';

const Message = ({message}) => (
  <div className="chat-message" style={{
    flexDirection: message.self ? 'row' : 'row-reverse'
  }}>
    <div className="chat-message-bubble" style={{
      backgroundColor: message.self ? '#C6E2FF' : '#fff'
    }}>
      {message.data}
    </div>
  </div>
);

const Notification = ({message}) => (
  <div></div>
)

export default class Chat extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      loadingStatus: 0,
      messages: [],
      pendingMessage: ""
    }

    this.socket = null;

    this.newMessage = this.newMessage.bind(this);
    this.chatScrollBottom = this.chatScrollBottom.bind(this);
  }

  componentDidMount() {
    // Start connecting
    this.socket = new WebSocket(`${config.WS_ADDRESS}/?topic=${this.props.topic}`);
    let that = this;

    this.socket.onmessage = function(event) {
      let json = JSON.parse(event.data);
      //console.log(json);
      if (json.type === 'meta') {
        if (json.data === 'matched') {
          that.setState({loading: false});
        }
        if (json.data === 'occupied') {
          alert("该频道已被占用人满，请更换频道");
          that.socket.close();
        }
      } 
      if (json.type === 'notification' && json.data === 'userleft') {
        alert("对方离开了频道，聊天结束");
        that.socket.close();
        //that.setState({
        //  messages: that.state.messages.concat([json])
        //});
        //that.chatScrollBottom();
      }
      if (json.type === 'message') {
        that.setState({
          messages: that.state.messages.concat([json])
        });
        that.chatScrollBottom();
      }
    };

    this.socket.onclose = function(event) {
      // TODO: maybe print something?
      // TODO: Ask user to click quit? 
      
      that.socket = null;
      //alert('Your pal lost connection :<');
      if (that.props.onClose) {
        that.props.onClose();
      }
    };
  }

  chatScrollBottom() {
    // Kind of ugly. but hey blame React
    setTimeout(() => {
      this.chatArea.scrollTop = this.chatArea.scrollHeight - this.chatArea.clientHeight
    }, 100);
  }

  newMessage(e) {
    e.preventDefault();

    this.chatScrollBottom();

    let messages = this.state.messages;
    let newm = this.state.pendingMessage;
    messages.push({
      self: true,
      data: newm
    });
    this.setState({
      pendingMessage: "",
      messages: messages
    });

    if (this.socket) {
      this.socket.send(JSON.stringify({
        type: 'message', 
        data: newm
      }));
    }
  }

  renderLoading() {
    return (
      <div className="chat-loading">
        <i 
          className="fa fa-spinner fa-spin"
        />
        <div className="loading-text">
          频道"{this.props.topic}"已就位，等待其他用户加入...
        </div>
      </div>
    );
  }

  renderChat() {
    return (
      <div className="chat-dialog pure-u-1 pure-u-md-2-3">
        <form className="control-bar pure-form">
          <input 
            value={this.state.pendingMessage}
            onChange={ (e) => {
              this.setState({
                pendingMessage: e.target.value
              });
            }}
            type="text" autoFocus/> 
          <button type="submit" 
            onClick={this.newMessage}
            className="pure-button pure-button-primary">
            <i className="fa fa-send"/>
          </button>
          <a className="pure-button">
            <i className="fa fa-navicon"/>
          </a>
        </form>
        <div className="chat-messages" ref={(div) => {this.chatArea = div }}>
          { this.state.messages.map((v, k) => {
            if (v.type === 'notification') {
              return (<Notification message={v} key={k}/>);
            } else {
              return (<Message message={v} key={k}/>);
            }
          }) }
        </div>
      </div>
    );
  }

  render() {

    return (
      <div className="chat-wrapper"> 
        { 
          this.state.loading ? 
            this.renderLoading() : 
            this.renderChat()
        }
      </div>
    );
  }
}
