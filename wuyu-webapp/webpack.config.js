const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');


module.exports = function(env) {
  const isDev = !env || !env.production;

  if (!isDev) {
    console.log('------- PRODUCTION MODE ENABLED -------');
  }

  const config = {
    entry: "./src/app.js", 

    output: {
      path: path.resolve(__dirname, "dist"),
      filename: "bundle.[hash].js"
    },

    devtool: isDev ? "source-map" : false, // TODO: maybe source-map is better

    resolve: {
      extensions: [".js", ".json", ".jsx"]
    },

    module: {
      rules: [{
        test: /\.jsx?$/,
        loader: "babel-loader",

        options: {
          presets: [
            ["es2015", {"modules": false}],
            "react"
          ],
        }
      }, {
        test: /\.pug$/, 
        loader: "pug-loader"
      }, {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract({
          fallbackLoader: "style-loader",
          loader: isDev ? "css-loader" : "css-loader?minimize"
        }),
        //use: [
        //  {loader: "style-loader"},
        //  {loader: "css-loader"} 
        //]
      }, {
        test: /\.(png|jpg|jpeg|gif|svg|woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader?limit=10000',
      }, {
        test: /\.(eot|ttf|wav|mp3)(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'file-loader',
      }],
    }, 

    plugins: [
      new ExtractTextPlugin("style.[contenthash].css"),
      new HTMLWebpackPlugin({
        template: 'index.pug'
      })
    ],

    externals: {
      "config": JSON.stringify(isDev ? {
        WS_ADDRESS: "ws://127.0.0.1:8081"
      } : {
        WS_ADDRESS: "wss://api.wuyu.ws"
      })
    }
  };

  if (!isDev) {
    config.plugins.push(new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }));
    config.plugins.push(new webpack.optimize.UglifyJsPlugin({
      compress: { warnings: false }
    }));
  }

  return config;
};
